<?php
header( 'Content-type: image/svg+xml' );

//	Set up Twitter Connection
require_once ('codebird.php');
\Codebird\Codebird::setBearerToken('');

$cb = \Codebird\Codebird::getInstance();


$id = $_GET["id"];
// $id = "1357833624029171719";
//	Get Tweet Data
$tweet_data = $cb->statuses_show_ID("id={$id}&tweet_mode=extended&include_ext_alt_text=true", true);

function twitter_parse_tags($input, $entities = false, $rel = false) {

	$replacements = array();
	$entities = (array)$entities;

	if (isset($entities['hashtags'])) {
		foreach ($entities['hashtags'] as $hashtag) {
			$hashtag = (array)$hashtag;
			list ($start, $end) = $hashtag['indices'];
			$replacements[$start] = array($start, $end,
				"<a href=\"https://twitter.com/hashtag/{$hashtag['text']}\">#{$hashtag['text']}</a>");
		}
	}
	if (isset($entities['urls'])) {
		foreach ($entities['urls'] as $url) {
			$url = (array)$url;
			$parsed_url = parse_url($url['expanded_url']);

			if (empty($parsed_url['scheme'])) {
				$url_full = 'http://' . $url['expanded_url'];
			} else{
				$url_full = $url['expanded_url'];
			}

			list ($start, $end) = $url['indices'];
			$replacements[$start] = array($start, $end,
				"<a href=\"{$url_full}\">{$url['display_url']}</a>");
		}
	}
	if (isset($entities['user_mentions'])) {
		foreach ($entities['user_mentions'] as $mention) {
			$mention = (array)$mention;
			list ($start, $end) = $mention['indices'];
			$replacements[$start] = array($start, $end,
				"<a href=\"https://twitter.com/{$mention['screen_name']}\">@{$mention['screen_name']}</a>");
		}
	}
	if (isset($entities['media'])) {
		foreach ($entities['media'] as $media) {
			$media = (array)$media;
			list ($start, $end) = $media['indices'];
			$replacements[$start] = array($start, $end, "");
		}
	}

	// sort in reverse order by start location
	krsort($replacements);

	foreach ($replacements as $replace_data) {
		list ($start, $end, $replace_text) = $replace_data;
		//	Twitter counts by CHARACTER - so you need a Multibyte aware PHP installation
		$input = mb_substr($input, 0, $start, 'UTF-8').$replace_text.mb_substr($input, $end, NULL, 'UTF-8');
	}

	//Linebreaks.  Some clients insert \n for formatting.
	$out =  nl2br($input);

	//Return the completed string
	return $out;
}

//	User header
$name = htmlspecialchars($tweet_data->user->name);
$screen_name = $tweet_data->user->screen_name;
$avatar = $tweet_data->user->profile_image_url_https;
$avatar_b64 = base64_encode(file_get_contents($avatar));

// Link back to original
$id = $tweet_data->id_str;
$url = "https://twitter.com/{$screen_name}/status/{$id}";

//	Timestamps
$created_at = $tweet_data->created_at;
$time = strtotime($created_at);
$date = new DateTime("@$time");
$iso_date = $date->format('c');

//	Body text
//	Should be the extended one
$text = $tweet_data->full_text;
$entities = $tweet_data->entities;
$body_html = twitter_parse_tags($text, $entities);

//	Media
$media_html = "";
if (isset($tweet_data->extended_entities)) {

	foreach($tweet_data->extended_entities->media as $media) {

		$image  = $media->media_url_https;
		$width  = $media->sizes->small->w;
		$height = $media->sizes->small->h;

		if (isset($media->ext_alt_text)){
			$alt = $media->ext_alt_text;
		}

		$media_url = $tweet_data->entities->media[0]->media_url_https;
		$media_b64 = base64_encode(file_get_contents("{$media_url}?name=small"));

		$media_html .= "<a href=\"{$url}\"><img
			class=\"media-tweetsvg\"
			width=\"{$width}\"
			src=\"data:image/jpeg;base64,{$media_b64}\"
			alt=\"{$alt}\"/></a>";
	}
}

$output = <<< EOT
<svg xmlns="http://www.w3.org/2000/svg"
	xmlns:xlink="http://www.w3.org/1999/xlink"
	width="20em">
<foreignObject x="0" y="0"
	width="20em" height="100%"
	fill="#eade52">
	<style>
		.tweetsvg{clear:none;}
		a.tweetsvg{color: rgb(27, 149, 224); text-decoration:none;}
		blockquote.tweetsvg{margin:0; background-color:#fefefe; border-radius:2%; border-style:solid; border-width:.1em; border-color:#ddd; padding:1em; font-family:sans; width:17rem}
		.avatar-tweetsvg{float:left; width:4rem; height:4rem; border-radius:50%;margin-right:.5rem;;margin-bottom:.5rem;border-style: solid; border-width:.1em; border-color:#ddd;}
		h1.tweetsvg{margin:0;font-size:1rem;text-decoration:none;color:#000;}
		h2.tweetsvg{margin:0;font-size:1rem;font-weight:normal;text-decoration:none;color:rgb(101, 119, 134);}
		p.tweetsvg{font-size:1rem; clear:both;}
		hr.tweetsvg{color:#ddd;}
		.media-tweetsvg{border-radius:2%; max-width:100%;border-radius: 2%; border-style: solid; border-width: .1em; border-color: #ddd;}
		time.tweetsvg{font-size:.9rem; font-family:sans; margin:0; padding-bottom:1rem;color:rgb(101, 119, 134);text-decoration:none;}
	</style>
	<blockquote class="tweetsvg" xmlns="http://www.w3.org/1999/xhtml">
		<img class="avatar-tweetsvg" alt="" src="data:image/jpeg;base64,{$avatar_b64}" />

		<a class="tweetsvg" href="{$url}"><h1 class="tweetsvg">{$name}</h1></a>

		<a class="tweetsvg" href="{$url}"><h2 class="tweetsvg">@{$screen_name}</h2></a>

		<p class="tweetsvg">{$body_html}</p>

		{$media_html}

		<a class="tweetsvg" href="{$url}">
			<time class="tweetsvg" datetime="{$iso_date}">{$created_at}</time>
		</a>
	</blockquote>
</foreignObject>
</svg>
EOT;
echo $output;
